# OpenSUSE Kalpa

This project is my repo to setup a fresh OpenSUSE Kalpa system up and running.

# Get OpenSUSE Kalpa iso image

```
https://www.opensuse.org/
```

```
https://get.opensuse.org/microos/
```

# Installer

Select partition and use guided setup to encrypt the laptop drive

Select time zone to correct the right date time

Select network configuration to change hostname

Under Security

enable firewall

enable ssh services

# Setup desktop environment

Setup my KDE plasma layout and import my global shorctuts with the kksrc file.

# Cloning this repo

```
git clone https://gitlab.com/dotlabstudio/kalpa.git
```

then cd to the repo, and

```
chmod +x *.sh
```

to use the scripts in this repo.


# Post Install

check repos list with

```
zypper repos
```

if there an installer repo - remove it with sudo zypper rr <#repo>

```
sudo zypper rr 1

sudo zypper ref
```

## Update the system

```
flatpak update -y; sudo zypper clean && sudo zypper ref && sudo transactional-update dup
```
then reboot.

## Change default shell to fish 

Check where fish is located with

```
which fish
```

then

```
chsh -s /usr/bin/fish
```

## Setup Tailscale

### need transactional-update shell to add new repos and install ms core fonts 

```
sudo transactional-update shell 
```

### Add the Tailscale repository:

```
sudo zypper ar -g -r https://pkgs.tailscale.com/stable/opensuse/tumbleweed/tailscale.repo
```

see https://tailscale.com/download/linux/opensuse-tumbleweed for reference.

### refresh the repos

```
sudo zypper ref
```

### exit the transactional-update shell
```
exit
``` 

or 

Use my kalpa.sh script to set my base packages for kalpa by cd to the cloned repo directory and run:

```
./kalpa.sh
```

After the reboot into the new snapshot

Use systemctl to enable and start the service tailscale:

```
sudo systemctl enable --now tailscaled
```

Connect the machine to the Tailscale network and authenticate in a web browser:

```
sudo tailscale up
```

see https://tailscale.com/download/linux/opensuse-tumbleweed

# Install my base image - this should only needed to once only - rest of the time - use flatpaks or distrobox

```
sudo transactional-update --non-interactive pkg install fish fastfetch htop yt-dlp eza bat lsd ouch gstreamer-1.20-plugin-openh264 git libvirt libvirt-daemon-qemu qemu-tools virt-install tailscale libvirt-daemon-lxc virt-manager ripgrep fira-code-fonts fontawesome-fonts procs sd tealdeer jetbrains-mono-fonts atuin k3b hunspell hunspell-tools aspell aspell-en lynis sysstat rkhunter helix clamav qemu-kvm osinfo-db-tools yajl gtk-vnc-tools spice-protocol-devel spice-gtk spice-up usbredir vde2 libvirglrenderer1 acpica ovmf bridge-utils qemu-hw-display-virtio-gpu qemu-hw-display-virtio-vga libvdpau_virtio_gpu qemu-hw-display-qxl qemu-block-gluster qemu-block-iscsi qemu-block-rbd libguestfs qemu-chardev-spice virtiofsd fortune pandoc-cli 7zip unrar dirmngr
```

note that curl, wget and vim are already installed on opensuse kalpa


## Setup virt manager
Use the virtualKalpa.sch script to start libvirt services.

After the script finishes;

edit the /etc/libvirt/libvirtd.conf file with

```
sudo vim /etc/libvirt/libvirtd.conf
```

Set the domain socket group ownership to libvirt

```
unix_sock_group = "libvirt"
```

Set the UNIX socket permissions for the R/W socket

```
unix_sock_ro_perms = "0777"

unix_sock_rw_perms = "0770"
```

Just uncomment these sections in the libvirtd.conf file

Lastly, setup the default virt manager network under kalpa by running:

```
sudo virsh net-edit default
```

insert the dns section into the file
```
<network>
...
  <dns>
    <forwarder addr='9.9.9.9'/>
    <forwarder addr='149.112.112.112'/>
  </dns>
...
</network>
```

see https://en.opensuse.org/Portal:MicroOS/Virtualization

### Setup kvm

```
sudo transactional-update pkg install -t pattern kvm_server kvm_tools

sudo transactional-update -c pkg install -y libvirt libvirt-client libvirt-daemon virt-manager virt-install virt-viewer qemu qemu-kvm qemu-ovmf-x86_64 qemu-tools
```

```
sudo transactional-update -c pkg install pciutils usbutils
```

And after successful installation of all packages and reboot, the libvirt service should be activated:

```
sudo systemctl enable --now libvirtd
```

Add user to the librvirt group 
```
sudo usermod -aG libvirt $USER
```

see https://github.com/cryinkfly/openSUSE-Baldur


## Setup virtualbox

```
sudo transactional-update --non-interactive pkg install virtualbox virtualbox-qt virtualbox-host-source kernel-devel kernel-default-devel gcc make
```

reboot 

### Not really needed 

```
sudo systemctl stop vboxdrv
```

need the sudo transactional-update shell for:

```
sudo vboxconfig
```

### Adding the user to vboxusers group is needed 

add user to the vboxusers group

```
sudo gpasswd -a $USER vboxusers
```

reboot 

### Install the the Extension Pack

need to sudo transactional-update shell for this:

```
wget https://download.virtualbox.org/virtualbox/7.0.14/Oracle_VM_VirtualBox_Extension_Pack-7.0.14.vbox-extpack

sudo VBoxManage extpack install --replace Oracle_VM_VirtualBox_Extension_Pack-7.0.14.vbox-extpack
```

Note that the 7.0.14 build is the latest virtualbox as 2024-02-15 for both tumbleweed repos and the virtualbox website

### Get the VBoxGuestAdditions iso before creating a virtual machine in virtualbox 

```
wget https://download.virtualbox.org/virtualbox/7.0.14/VBoxGuestAdditions_7.0.14.iso
```

## Setup new ssh pair keys for a new machine and copy public key to a server

To generate a new ssh pair keys on on a new machine:

```
ssh-keygen -t ed25519 -a 100
```

To copy public ssh keys to a server

```
ssh-copy-id -i ~/.ssh/id_ed25519.pub username@<serverIPorName>
```

# Distrobox

To use command line packages for one off time, use distrobox.

To create a tumbleweed distrobox:

```
distrobox create --name pkg --image registry.opensuse.org/opensuse/distrobox-packaging:latest 
```

To update all containers

```
distrobox-upgrade --all
```

or

```
distrobox upgrade -a
```

# Other distrobox containers

```
distrobox create --name arch --image quay.io/toolbx/arch-toolbox:latest

distrobox create --name debian --image quay.io/toolbx-images/debian-toolbox:12

distrobox create --name kali --image docker.io/kalilinux/kali-rolling:latest
```


# DevBox

To manage nix packages.

## Install nix

first run

```
sudo transactional-update run mksubvolume /nix
```

then
reboot

change the file permissions on /nix with:

```
sudo chown -R $USER /nix
```

Temporary set selinux to permissive mode until the next reboot

```
sudo setenforce 0
```

or

```
sudo setenforce permissive
```

then run the normal nix installer script:
```
curl -L https://nixos.org/nix/install | sh
```

Reboot

check selinux still enable and enforcing with:

``` 
sestatus
```

or 

```
sudo getenforce
```

### Check nix is working by updating it

```
nix-channel --list

nix-channel --update
```

## Install DevBox

```
curl -fsSL https://get.jetpack.io/devbox | bash
```

# Using DevBox like nix-shell

Create a new empty directory like devbox or project

cd into that directory

Initialize Devbox:

```
devbox init
```

This creates a devbox.json file in the current directory. 

Search for packages to add to the Devbox project with

``` 
devbox search <packages>
```

Add a package to the project by running:

```
devbox add <package>.
```

To remove a package:

```
devbox rm <package>
```

devbox.json file keeps track of the packages that added.

Start a new shell that has the packages and tools installed:

```
devbox shell
```

This is like the nix-shell

To exit the Devbox shell and return to the regular shell:

```
exit
```

see https://www.jetpack.io/devbox/docs/quickstart/

# Using DevBox as a primary package manager like home manager

To install global packages:

```
devbox global add <packages>
```

To remove global packages:

```
devbox global rm <packages>
```

To view a full list of global packages, run:

```
devbox global list
```

## Using Global Packages in the Host Shell
To make the global packages available in the host shell, add them to the shell PATH by running:

```
devbox global shellenv
```

will print the command necessary to source the packages.

see https://www.jetpack.io/devbox/docs/devbox_global/

## Using Fleek with DevBox Global

```
devbox global pull https://devbox.getfleek.dev/high
```

View the full list of scripts using devbox global run with no arguments

### Global sub commands

devbox global add - Add a global package to devbox

devbox global list - List global packages

devbox global pull - Pulls a global config from a file or URL

devbox global rm - Remove a global package

devbox global shellenv - Print shell commands that add global Devbox packages to your PATH


see https://www.jetpack.io/devbox/docs/cli_reference/devbox_global/

# Default global path 

```
~/.local/share/devbox/global/default
```

or run

```
devbox global path
```

# Nvidia drivers

```
https://en.opensuse.org/SDB:NVIDIA_drivers
```

Add repo:

```
sudo zypper addrepo --refresh https://download.nvidia.com/opensuse/tumbleweed NVIDIA

sudo zypper ref
```

## Get the hardware information

```
sudo lspci | grep VGA
sudo lscpu | grep Arch

sudo hwinfo --gfxcard | grep Model
sudo hwinfo --arch

sudo inxi -G
sudo inxi -Ga
```

## Search for nvidia cards

G03 = driver v340 = legacy driver for GT8xxx/9xxx devices (via a community user, see below)

G04 = driver v390 = legacy driver for GTX4xx/5xx Fermi devices

G05 = driver v470 = driver for GeForce 600 series

G06 = driver for GeForce 700 series (Kepler) and up (Maxwell, Pascal...)

for example:

```
sudo zypper se x11-video-nvidiaG0* nvidia-video-G06*

sudo zypper se -s x11-video-nvidiaG0* nvidia-video-G06*
```

search for cuda:

```
zypper se nvidia-gl*G0*
```

## Install nvidia drivers

For nvidia-video-G06 :
```
sudo transactional-update -i pkg in nvidia-driver-G06-kmp-default nvidia-video-G06 nvidia-gl-G06 nvidia-compute-G06
```

For x11-video-nvidiaG05 :
```
sudo transactional-update -i pkg in nvidia-gfxG05-kmp-default x11-video-nvidiaG05 nvidia-glG05 nvidia-computeG05
```

For x11-video-nvidiaG04 :
```
sudo transactional-update -i pkg in nvidia-gfxG04-kmp-default x11-video-nvidiaG04 nvidia-glG04 nvidia-computeG04
```

# Quad9 DNS

```
https://www.quad9.net/
```

for the browsers:
```
https://dns.quad9.net/dns-query
```

To check that quad9 is setup:
```
https://on.quad9.net 
```

# To cleanup snapshots

```
sudo snapper cleanup number
```

might need to run it multiple times.


To check disk space:

```
df -Hk
```

# Microsoft Core Fonts

Use sudo transactional-update shell to install or manually install with

```
sudo transactional-update pkg install fetchmsttfonts
```

# Tealdeer Setup

```
tldr  --update
```

# Atuin Setup

```
atuin import auto
```

# Dolphin Context Menu

Under Settings

Configure Dolphin

Context Menu

Download New services

search for


ark stuff or extract for archives 

and

meld for file comparsions 

# Homebrew

## Get list of applcations installed on aurora-dx (KDE version of bluefin-dx) with Homebrew

```
brew bundle dump --describe
```


## To restore aurora-dx homebrew apps from a Brewfile

cd into where the Brewfile is located first

```
brew bundle -v ; brew cleanup ; brew doctor 
```


from https://twit.tv/shows/hands-on-mac/episodes/9

```
brew bundle install --file /path/to/Brewfile
```

# Rollbacks on OpenSUSE tumbleweed & kalpa


Boot up machine and go to the read only snapshot and select the last working snapshot

usually the last time on yesterday date.

then go to konsole and type

```
sudo snapper rollback 
```

on Kalpa, you can also use 

```
sudo transactional-update rollback last
```

and reboot 

# Getting to a tty 

logout of wayland or x11 session

Ctrl+Alt+F4

or

Ctrl+Alt+F2

# Keep rolling with mega plasma updates

Check to see if there missing packages for:

- sddm 

- plasma 

- wayland

- kcm

- kwallet 

for example, after the plasma 6 update on the tumbleweed snapshot, on the login screen, switch to tty
and run the following command:

```
sudo transactional-update pkg install sddm-qt6 plasma6-session-x11 plasma-wayland-protocols pam_kwallet6
```


Personal Plasma 6 Settings

- Change back to SINGLE CLICK

- Change panel to floating

- In Dolphin, F9 or
  Show --->  Show Panels ---> Places

# Bazzite/Aurora-dx 

for computers with burners on them:
```
rpm-ostree install k3b cdrskin libburn
```

bazzite rpm-ostree:
```
rpm-ostree install cdrskin edk2-ovmf k3b libburn qemu virt-manager
```

# Three buttons touchpad/mouse Rules

middle button for paste is a game changer

# Fonts under Linux and Bazzitte

Install or copy fonts to:
```
~/.local/share/fonts/
```

create the fonts folder:
```
mkdir -p ~/.local/share/fonts/
```

Finally, update the fontconfig cache (usually unnecessary as software using the fontconfig library does this):
```
fc-cache
```

source:
https://wiki.archlinux.org/title/Fonts#Manual_installation





