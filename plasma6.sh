#!/usr/bin/env bash

echo "This script add the missing packages for plasma 6 on kalpa"

sudo transactional-update pkg install sddm-qt6 plasma6-session-x11 plasma-wayland-protocols pam_kwallet6
