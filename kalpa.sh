#!/usr/bin/env bash

# This is my post install script for opensuse kalpa. It sets up tailscale, my default shell, my rust cli tools
# and virt manager

# need transactional-update shell for adding new repos and ms core fonts

sudo transactional-update shell 

# Add the Tailscale repository:

sudo zypper ar -g -r https://pkgs.tailscale.com/stable/opensuse/tumbleweed/tailscale.repo

# refresh the repos

sudo zypper ref

# exit the transactional-update shell
exit 

# install my base image - this should only needed to once only - rest of the time - use flatpaks or distrobox

sudo transactional-update --non-interactive pkg install fish fastfetch htop yt-dlp eza bat lsd ouch gstreamer-1.20-plugin-openh264 git libvirt libvirt-daemon-qemu qemu-tools virt-install tailscale libvirt-daemon-lxc virt-manager ripgrep fira-code-fonts fontawesome-fonts procs sd tealdeer jetbrains-mono-fonts atuin k3b hunspell hunspell-tools aspell aspell-en lynis sysstat rkhunter helix clamav qemu-kvm osinfo-db-tools yajl gtk-vnc-tools spice-protocol-devel spice-gtk spice-up usbredir vde2 libvirglrenderer1 acpica ovmf bridge-utils qemu-hw-display-virtio-gpu qemu-hw-display-virtio-vga libvdpau_virtio_gpu qemu-hw-display-qxl qemu-block-gluster qemu-block-iscsi qemu-block-rbd libguestfs qemu-chardev-spice virtiofsd fortune pandoc-cli 7zip unrar dirmngr

# curl, wget and vim are already installed on opensuse kalpa

