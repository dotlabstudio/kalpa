#!/usr/bin/env bash

# install my base image - this should only needed to once only - rest of the time - use flatpaks or distrobox

echo "add tailsacle repos first"

sudo transactional-update --non-interactive pkg install fish neofetch htop yt-dlp eza bat lsd ouch gstreamer-1.20-plugin-openh264 git libvirt libvirt-daemon-qemu qemu-tools virt-install tailscale libvirt-daemon-lxc virt-manager ripgrep fira-code-fonts fontawesome-fonts procs sd tealdeer jetbrains-mono-fonts atuin k3b hunspell hunspell-tools aspell aspell-en lynis sysstat rkhunter helix clamav

# curl, wget and vim are already installed on opensuse kalpa

